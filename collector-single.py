# aiohttp rate limiting by gist:
# https://gist.github.com/pquentin/5d8f5408cdad73e589d85ba509091741

import sqlite3 as lite
import json
import datetime
import humanize
import asyncio
import time
import aiohttp
import sys
from pathlib import Path
import os.path
import os
import gzip


os.chdir(os.path.dirname(os.path.abspath(__file__)))

with open("data/config.json") as json_data_file:
    cfg = json.load(json_data_file)

token = cfg["WargamingCollectorToken"]


class RateLimiter:
    """Rate limits an HTTP client that would make get() and post() caAAlls.

    Calls are rate-limited by host.
    https://quentin.pradet.me/blog/how-do-you-rate-limit-calls-with-aiohttp.html
    This class is not thread-safe."""

    RATE = 20
    MAX_TOKENS = 20

    def __init__(self, client):
        self.client = client
        self.tokens = self.MAX_TOKENS
        self.updated_at = time.monotonic()

    async def get(self, *args, **kwargs):
        await self.wait_for_token()
        return self.client.get(*args, **kwargs)

    async def wait_for_token(self):
        while self.tokens < 1:
            self.add_new_tokens()
            await asyncio.sleep(1)
        self.tokens -= 1

    def add_new_tokens(self):
        now = time.monotonic()
        time_since_update = now - self.updated_at
        new_tokens = time_since_update * self.RATE
        if new_tokens > 1:
            self.tokens = min(self.tokens + new_tokens, self.MAX_TOKENS)
            self.updated_at = now


async def main():

    async with aiohttp.ClientSession(loop=loop) as client:
        client = RateLimiter(client)

        async def get_files(url, file_name):
            async with await client.get(url) as resp:
                data = await resp.json()
                for acc_id, acc_data in data["data"].items():
                    filename = "stats/daily/{}-{}.txt".format(acc_id, file_name)
                    with gzip.open(filename, "w") as f:
                        f.write(json.dumps(acc_data).encode("utf-8"))

        async def get_single_file(url, file_name, acc_id):
            filename = "stats/daily/{}-{}.txt".format(acc_id, file_name)
            async with await client.get(url) as resp:
                with gzip.open(filename, "w") as f:
                    f.write(json.dumps(await resp.json()).encode("utf-8"))

        u = [{"account_id": "534883764"}]
        region = "eu"
        for i in u:

            url = "https://api.wotblitz.{}/wotb/account/info/?application_id={}&account_id={}&extra=statistics.rating".format(
                region, token, i["account_id"]
            )
            filename = "account_info"
            try:
                await get_files(url, filename)
            except Exception as e:
                print(e)

            url = "https://api.wotblitz.{}/wotb/account/achievements/?application_id={}&account_id={}".format(
                region, token, i["account_id"]
            )
            filename = "achievements"
            try:
                await get_files(url, filename)
            except Exception as e:
                print(e)

        for i in u:

            url = "https://api.wotblitz.{}/wotb/tanks/stats/?application_id={}&account_id={}".format(
                region, token, i["account_id"]
            )
            filename = "tank_stats"
            try:
                await get_single_file(url, filename, i["account_id"])
            except Exception as e:
                print(e)

            url = "https://api.wotblitz.{}/wotb/tanks/achievements/?application_id={}&account_id={}".format(
                region, token, i["account_id"]
            )
            filename = "vehicle_achievements"
            try:
                await get_single_file(url, filename, i["account_id"])
            except Exception as e:
                print(e)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()

    loop.run_until_complete(main())
