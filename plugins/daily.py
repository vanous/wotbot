import discord
from discord.ext import commands
from discord import colour
import asyncio
from operator import itemgetter
import random

import json
from tabulate import tabulate
from collections import Counter
from operator import itemgetter
from collections import OrderedDict
import pickle
import datetime
from pathlib import Path
import humanize
import aiohttp
import math
import gzip
import os
import zlib


class UserStats(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def get_new_data(self, player_id, region, token=None, ownuser=False):
        # print("get new data", token, ownuser)
        new_data = {}

        veh_achievs = await self.bot.wg.get_wg(
            region=region,
            cmd_group="tanks",
            cmd_path="achievements",
            parameter="account_id={}".format(player_id),
        )

        veh_achievs_new = {}
        if not veh_achievs[player_id]:
            return None
        for a in veh_achievs[player_id]:
            veh_achievs_new[a["tank_id"]] = a

        new_data["veh_achievs"] = veh_achievs_new

        info_data_new = False
        if ownuser and token is not None:
            info_data_new = await self.bot.wg.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="info",
                parameter="account_id={}&extra=statistics.rating&access_token={}&fields=nickname,statistics.all,statistics.rating,private".format(
                    player_id, token
                ),
                access_token=token,
            )

        new_data["token_expired"] = False
        if ownuser and not info_data_new:
            new_data["token_expired"] = True

        if not ownuser or not info_data_new:

            info_data_new = await self.bot.wg.get_wg(
                region=region,
                cmd_group="account",
                cmd_path="info",
                parameter="account_id={}&extra=statistics.rating".format(player_id),
            )

        if not info_data_new:
            return None

        private_data_new = None
        private_data_new = info_data_new[str(player_id)].get("private", None)

        new_data["private_data"] = private_data_new

        # new_data["nickname"] = info_data_new[str(player_id)].get("nickname", None)
        info_data_new = info_data_new[str(player_id)].get("statistics", None)
        # if info_data_new:
        #   info_data_new = info_data_new.get("rating", None)

        new_data["info_data"] = info_data_new

        # print("aaa", new_data["private_data"])

        achievs_new = await self.bot.wg.get_wg(
            region=region,
            cmd_group="account",
            cmd_path="achievements",
            parameter="account_id={}".format(player_id),
        )
        new_data["achievs"] = achievs_new[player_id]

        player_tanks = await self.bot.wg.get_wg(
            region=region,
            cmd_group="tanks",
            cmd_path="stats",
            parameter="account_id={}".format(player_id),
        )
        player_tanks_new = {}
        for t in player_tanks[player_id]:
            player_tanks_new[t["tank_id"]] = t

        new_data["player_tanks"] = player_tanks_new

        if new_data["player_tanks"] is None:
            return None

        return new_data

    def get_old_data(self, all_data, player_id, when):
        old_data = {}

        if all_data["{}_tank_stats".format(when)] is None:
            return None
        file_date = datetime.datetime.strptime(
            all_data["{}_date".format(when)], "%Y-%m-%d %H:%M:%S.%f"
        )

        pto = json.loads(zlib.decompress(all_data["{}_tank_stats".format(when)]))
        if not pto.get("data", False):
            return None
        pto = pto["data"][player_id]
        player_tanks_old = {}
        if not pto:
            return None

        for t in pto:
            player_tanks_old[t["tank_id"]] = t
        old_data["player_tanks"] = player_tanks_old
        old_data["file_date"] = file_date

        if all_data["today_vehicle_achievements"] is None:
            return None

        veh_achievs = json.loads(
            zlib.decompress(all_data["today_vehicle_achievements"])
        )
        # print("VA:",veh_achievs)
        if not veh_achievs.get("data", False):
            return None
        veh_achievs = veh_achievs["data"][player_id]
        veh_achievs_old = {}
        if not veh_achievs:
            return None
        for a in veh_achievs:
            veh_achievs_old[a["tank_id"]] = a

        old_data["veh_achievs"] = veh_achievs_old

        if all_data["{}_account_info".format(when)] is None:
            return None

        info_data = json.loads(
            zlib.decompress(all_data["{}_account_info".format(when)])
        )
        # print("old info data", info_data)
        private_data = None
        private_data = info_data.get("private", None)

        old_data["private_data"] = private_data

        info_data = info_data.get("statistics", None)
        # if info_data:
        #    info_data = info_data.get("rating", None)

        old_data["info_data"] = info_data

        if all_data["{}_achievements".format(when)] is None:
            return None

        achievs = json.loads(zlib.decompress(all_data["{}_achievements".format(when)]))

        old_data["achievs"] = achievs

        return old_data

    async def calc_daily(self, player, region, ctx, when, token=None, ownuser=False):
        # self.bot.logger.info(("calculating", when))
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        if when == "today":
            when_old = "today"
        elif when == "yesterday":
            when_old = "yesterday"
        elif when == "twodays":
            when_old = "yesterday"

        all_vehicles = await self.bot.wg.get_all_vehicles()
        player_id = str(player[0]["account_id"])
        player_nick = player[0]["nickname"]

        # self.bot.logger.info(("daily", player_id))

        stored_data = await self.bot.sqlite.get_daily_user(player_id)

        if stored_data is None:
            try:
                await ctx.send(
                    _(
                        "No saved data for `{}@{}`. Saving will start from now on, try again tomorrow."
                    ).format(player_nick, region)
                )

            except discord.Forbidden:
                await ctx.message.author.send(
                    _(
                        "No saved data for `{}@{}`. Saving will start from now on, try again tomorrow."
                    ).format(player_nick, region)
                )

            return None, None, None

        old_data = self.get_old_data(stored_data, player_id, when_old)
        if old_data is None:
            await ctx.send(
                _("No data from WG for `{}@{}` ").format(player_nick, region)
            )
            return None, None, None

        color_tier = "json"
        color_tanks = "css"
        color_total = "json"
        color_achievs = "json"
        veh_achievs_old = old_data["veh_achievs"]
        player_tanks_old = old_data["player_tanks"]
        file_date = old_data["file_date"]
        achievs_old = old_data["achievs"]
        info_data_old = old_data["info_data"].get("rating", None)

        if when == "twodays":
            new_data = await self.get_new_data(
                player_id, region, token=token, ownuser=ownuser
            )
        elif when == "today":
            new_data = await self.get_new_data(
                player_id, region, token=token, ownuser=ownuser
            )
        elif when == "yesterday":
            new_data = self.get_old_data(stored_data, player_id, "today")

        if new_data is None:
            await ctx.send(
                _("No data from WG for `{}@{}` ").format(player_nick, region)
            )
            return None, None, None
        # print("new data", new_data["info_data"])
        veh_achievs_new = new_data["veh_achievs"]
        info_data_new = new_data["info_data"].get("rating", None)
        achievs_new = new_data["achievs"]
        player_tanks_new = new_data["player_tanks"]

        # self.bot.logger.info("old: {}".format(old_data["private_data"]))
        # self.bot.logger.info("new: {}".format(new_data["private_data"]))

        private_old = old_data["private_data"]
        private_new = new_data["private_data"]

        # self.bot.logger.info(new_data.get("nickname"))

        # if new_data.get("nickname") is not None:
        #    player_nick = new_data.get("nickname")
        res_embed = None
        if private_old is not None and private_new is not None:
            # private_data_diff = Counter(old_data["private_data"]).subtract(Counter(
            #    new_data["private_data"]
            # ))
            private_diff = {}
            for i in ["gold", "free_xp", "credits"]:
                private_diff[i] = private_new.get(i, 0) - private_old.get(i, 0)
            # private_diff_out = {k: v for k, v in private_diff.items() if v > 0}

            if ownuser:
                # print(private_diff, player_nick)

                when_list = {
                    "today": _("Today"),
                    "yesterday": _("Yesterday"),
                    "twodays": _("Today & Yesterday"),
                }
                res_embed = discord.Embed(
                    title="{m} {when}".format(
                        when=when_list[when], m=_("Daily resources stats for:")
                    ),
                    description=_("Credits, gold and free XP."),
                    colour=234,
                    type="rich",
                )
                # embed.set_author(name="WotBot", url="https://wotbot.pythonanywhere.com/", icon_url="https://cdn.discordapp.com/attachments/414445407729352704/526154361685016586/maxresdefault.jpg")

                res_embed.set_footer(
                    text=_(
                        "Stats by \U0001f451 WotBot. Resets early morning for each region. Credits slow update by WarGaming."
                    )
                )

                if private_new.get("is_premium", False):
                    res_embed.add_field(
                        inline=False,
                        name=_("Premium account active"),
                        value=_("\U000023f2 Expires in {}.").format(
                            humanize.naturaltime(
                                datetime.datetime.fromtimestamp(
                                    (private_new.get("premium_expires_at", 0))
                                )
                            )
                        ),
                    )

                res_embed.add_field(name="\uFEFF", value="\uFEFF", inline=False)
                res_embed.add_field(
                    name=_("Current values:"), value="\uFEFF", inline=False
                )

                res_embed.add_field(
                    inline=True,
                    name=_("Gold"),
                    value="\U0001f4b0 {0:,d}".format(
                        private_new.get("gold", 0)
                    ).replace(",", " "),
                )
                res_embed.add_field(
                    inline=True,
                    name=_("Credits"),
                    value="\U0001f48e {0:,d}".format(
                        private_new.get("credits", 0)
                    ).replace(",", " "),
                )
                res_embed.add_field(
                    inline=True,
                    name=_("Free XP"),
                    value="\U0001f4b5 {0:,d}".format(
                        private_new.get("free_xp", 0)
                    ).replace(",", " "),
                )

                res_embed.add_field(name="\uFEFF", value="\uFEFF", inline=False)
                res_embed.add_field(
                    name=_("Difference for: {when}").format(when=when_list[when]),
                    value="\uFEFF",
                    inline=False,
                )

                res_embed.add_field(
                    inline=True,
                    name=_("Gold"),
                    value="\U0001f4b0 {0:+,d}".format(
                        private_diff.get("gold", 0)
                    ).replace(",", " "),
                )
                res_embed.add_field(
                    inline=True,
                    name=_("Credits"),
                    value="\U0001f48e {0:+,d}".format(
                        private_diff.get("credits", 0)
                    ).replace(",", " "),
                )
                res_embed.add_field(
                    inline=True,
                    name=_("Free XP"),
                    value="\U0001f4b5 {0:+,d}".format(
                        private_diff.get("free_xp", 0)
                    ).replace(",", " "),
                )

        mastery_table = None

        mastery_table = {}
        for veh_achiev in player_tanks_new.values():
            if veh_achiev.get("mark_of_mastery", None) is not None:
                veh_achiev_old = player_tanks_old.get(veh_achiev["tank_id"], None)
                old_mastery = None
                if veh_achiev_old is not None:
                    if veh_achiev_old.get("mark_of_mastery", None) is not None:
                        old_mastery = veh_achiev_old["mark_of_mastery"]
                if veh_achiev["mark_of_mastery"] != old_mastery:
                    if veh_achiev["mark_of_mastery"] != 4:
                        if veh_achiev["mark_of_mastery"] in mastery_table:
                            mastery_table[veh_achiev["mark_of_mastery"]] = (
                                int(mastery_table[veh_achiev["mark_of_mastery"]]) + 1
                            )
                        else:
                            mastery_table[veh_achiev["mark_of_mastery"]] = 1

        # player_tanks_achievements = player_tanks_achievements[player_id]
        # print(veh_achievs_old)
        # player_tanks_achievements = {
        #    x["tank_id"]: x["achievements"] for x in veh_achievs_old
        # }

        def get_mastery_list(player_tanks, player_tanks_achievements):
            all_tanks = {}
            for tank_id, tank in player_tanks.items():

                markOfMastery = tank.get("mark_of_mastery", 0)
                if markOfMastery == 4:
                    if (
                        player_tanks_achievements[tank_id]["achievements"].get(
                            "markOfMastery", 0
                        )
                        != 4
                    ):
                        markOfMasteryCount = player_tanks_achievements[tank_id][
                            "achievements"
                        ].get("markOfMastery", 0)
                    else:
                        # best effort detection
                        markOfMasteryI = player_tanks_achievements[tank_id][
                            "achievements"
                        ].get("markOfMasteryI", None)
                        markOfMasteryII = player_tanks_achievements[tank_id][
                            "achievements"
                        ].get("markOfMasteryII", None)
                        markOfMasteryIII = player_tanks_achievements[tank_id][
                            "achievements"
                        ].get("markOfMasteryIII", None)
                        if (
                            markOfMasteryI is None
                            and markOfMasteryII is None
                            and markOfMasteryIII is None
                        ):
                            markOfMasteryCount = 1  # best effort
                        else:
                            markOfMasteryCount = player_tanks_achievements[tank_id][
                                "achievements"
                            ].get("markOfMastery", 0)

                    all_tanks[tank_id] = markOfMasteryCount
            return all_tanks

        try:
            old_m = get_mastery_list(player_tanks_old, veh_achievs_old)
            new_m = get_mastery_list(player_tanks_new, veh_achievs_new)
            diff_m = Counter(new_m) - Counter(old_m)
            for v in diff_m.values():
                if 4 in mastery_table:
                    mastery_table[4] = int(mastery_table[4]) + v
                else:
                    mastery_table[4] = v
        except Exception as e:
            print("daily mastery calc error", e)

        # if info_data_old and info_data_new:

        #    rating_diff = {
        #        k: info_data_new.get(k, 0) - info_data_old.get(k, 0)
        #        for k in set(info_data_old) | set(info_data_new)
        #    }
        #    if rating_diff["battles"]:
        #        try:
        #            rating_winrate = round(
        #                rating_diff["wins"] / float(rating_diff["battles"]) * 100, 2
        #            )
        #            rating_dpb = round(
        #                rating_diff["damage_dealt"] / float(rating_diff["battles"]), 2
        #            )
        #            rating_table = _(
        #                "Battles: {battles}, WR: {winrate}, DPB: {dpb}"
        #            ).format(
        #                battles=rating_diff.get("battles", 0),
        #                winrate=rating_winrate,
        #                dpb=rating_dpb,
        #            )
        #        except ArithmeticError as e:
        #            self.bot.logger.info(e)

        rating_table = None

        if info_data_old and info_data_new:

            rating_diff = {
                k: info_data_new.get(k, 0) - info_data_old.get(k, 0)
                for k in set(info_data_old) | set(info_data_new)
            }
            if rating_diff["battles"]:
                try:
                    rating_winrate = rating_diff["wins"] / rating_diff["battles"] * 100

                    rating_dpb = rating_diff["damage_dealt"] / rating_diff["battles"]

                    old_wins = info_data_old.get("wins", 0)
                    old_battles = info_data_old.get("battles", 0)
                    old_damage = info_data_old.get("damage_dealt", 0)

                    new_wins = info_data_new.get("wins", 0)
                    new_battles = info_data_new.get("battles", 0)
                    new_damage = info_data_new.get("damage_dealt", 0)

                    old_wr = old_wins / old_battles * 100
                    old_dpb = old_damage / old_battles

                    new_wr = new_wins / new_battles * 100
                    new_dpb = new_damage / new_battles

                    # print(old_wr, new_wr, old_dpb, new_dpb)

                    rating_diff["delta_wr"] = new_wr - old_wr
                    rating_diff["delta_dpb"] = new_dpb - old_dpb

                    rating_table = _(
                        "Battles: {battles} WR: {winrate:.2f} ({delta_wr:+,.2f}) DPB: {dpb:.1f} ({delta_dpb:+,.2f}) XP: {xp}"
                    ).format(
                        battles=rating_diff.get("battles", 0),
                        xp=rating_diff.get("xp", 0),
                        winrate=rating_winrate,
                        dpb=rating_dpb,
                        delta_wr=rating_diff.get("delta_wr", 0),
                        delta_dpb=rating_diff.get("delta_dpb", 0),
                    )
                except ArithmeticError as e:
                    self.bot.logger.info(e)

        achievs_table = None

        achievs_diff = Counter(achievs_new["achievements"]) - Counter(
            achievs_old["achievements"]
        )
        max_series_diff = Counter(achievs_new["max_series"]) - Counter(
            achievs_old["max_series"]
        )

        achievements_wg = await self.bot.wg.get_all_achievements()
        averages_all = await self.bot.wg.get_all_averages()

        achievs_table = []
        # for i, v in achievs_diff.items():
        #    achievs_table.append(
        #        [
        #            achievements_wg[i]["name"]
        #            .replace(achievements_wg[i]["achievement_id"], "")
        #            .replace("()", ""),
        #            v,
        #        ]
        #    )

        for i, v in max_series_diff.items():
            achievs_table.append(
                [
                    achievements_wg[i]["name"]
                    .replace(achievements_wg[i]["achievement_id"], "")
                    .replace("()", ""),
                    v,
                ]
            )

        averages_all = await self.bot.wg.get_all_averages()
        all_tanks = {}
        player_tanks_dict = {}
        t = {"battles": 0, "dmg": 0, "spots": 0, "kills": 0, "dcp": 0, "wins": 0}
        a = {"battles": 0, "dmg": 0, "spots": 0, "kills": 0, "dcp": 0, "wins": 0}
        for tank in player_tanks_new.values():

            player_tanks_dict[tank["tank_id"]] = tank["all"]

            if averages_all.get(str(tank["tank_id"]), None):
                ta = tank["all"]
                s = averages_all[str(tank["tank_id"])]

                t["battles"] = t["battles"] + ta["battles"]
                t["dmg"] = t["dmg"] + ta["damage_dealt"]
                t["spots"] = t["spots"] + ta["spotted"]
                t["kills"] = t["kills"] + ta["frags"]
                t["dcp"] = t["dcp"] + ta["dropped_capture_points"]
                t["wins"] = t["wins"] + ta["wins"]

                a["battles"] = a["battles"] + s["battles"]
                a["dmg"] = a["dmg"] + s["dmg"]
                a["spots"] = a["spots"] + s["spots"]
                a["kills"] = a["kills"] + s["kills"]
                a["dcp"] = a["dcp"] + s["dcp"]
                a["wins"] = a["wins"] + s["wins"]

            if str(tank["tank_id"]) in all_vehicles:
                tier = all_vehicles[str(tank["tank_id"])]["tier"]
                name = all_vehicles[str(tank["tank_id"])]["name"]

            else:
                tier = 0
                name = "Not in tankopedia"

            old_tank = player_tanks_old.get(tank["tank_id"], None)

            if old_tank is not None:
                all_diff = Counter(tank["all"]) - Counter(old_tank["all"])
            else:
                all_diff = tank["all"]
            if all_diff:
                if tier not in all_tanks:
                    all_tanks[tier] = {}

                all_tanks[tier][tank["tank_id"]] = {}
                all_tanks[tier][tank["tank_id"]]["all"] = all_diff
                all_tanks[tier][tank["tank_id"]]["name"] = name
        WN8 = 0
        try:
            rDAMAGE = t["dmg"] / ((a["dmg"] / a["battles"]) * t["battles"])
            rSPOT = t["spots"] / ((a["spots"] / a["battles"]) * t["battles"])
            rFRAG = t["kills"] / ((a["kills"] / a["battles"]) * t["battles"])
            rDEF = t["dcp"] / ((a["dcp"] / a["battles"]) * t["battles"])
            rWIN = t["wins"] / ((a["wins"] / a["battles"]) * t["battles"])

            rWINc = max(0, (rWIN - 0.71) / (1 - 0.71))
            rDAMAGEc = max(0, (rDAMAGE - 0.22) / (1 - 0.22))
            rFRAGc = max(0, min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12)))
            rSPOTc = max(0, min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38)))
            rDEFc = max(0, min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10)))
            WN8 = (
                980 * rDAMAGEc
                + 210 * rDAMAGEc * rFRAGc
                + 155 * rFRAGc * rSPOTc
                + 75 * rDEFc * rFRAGc
                + 145 * min(1.8, rWINc)
            )
        except:
            WN8 = 0
            self.bot.logger.info("1 WN8 err")

        when_list = {
            "today": _("Today"),
            "yesterday": _("Yesterday"),
            "twodays": _("Today & Yesterday"),
        }

        rating2 = []

        player_rating, player_rating_place, player_rating_league = await self.bot.wg.get_rating(
            player_id, region, ctx, score=2
        )

        def is_number(s):
            try:
                float(s)
                return True
            except ValueError:
                return False

        if is_number(player_rating):
            rating2 = [player_rating, player_rating_place, player_rating_league]

        title_text = _("Daily stats")
        embed = discord.Embed(
            title="{title} {for_} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                title=title_text,
                for_=_("for"),
                nick=player_nick,
                region=region,
                confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                contributor=await self.bot.dc.contributor(ctx, player, region),
            ),
            description=_(
                "Stats counted for: {when}. (Since {since}, {period}.)"
            ).format(
                period=humanize.naturaltime(datetime.datetime.now() - file_date),
                since=humanize.naturaldate(file_date),
                when=when_list[when],
            ),
            colour=234,
            url=await self.bot.dc.confirmed_url(ctx, ownuser),
            type="rich",
        )
        embed_short = discord.Embed(
            title="{title} {for_} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                title=title_text,
                for_=_("for"),
                nick=player_nick,
                region=region,
                confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                contributor=await self.bot.dc.contributor(ctx, player, region),
            ),
            description=_(
                "{}\nDiscord message length limit reached. Showing total stats only."
            ).format(""),
            colour=234,
            url=await self.bot.dc.confirmed_url(ctx, ownuser),
            type="rich",
        )

        embed_short.add_field(
            name="\uFEFF",
            value=_("Stats counted for: {when}. (Since {since}, {period}.)").format(
                period=humanize.naturaltime(datetime.datetime.now() - file_date),
                since=humanize.naturaldate(file_date),
                when=when_list[when],
            ),
        )

        winrate_for_color = 0
        a_t = OrderedDict(all_tanks)
        if a_t:
            all_total = Counter()
            char_counter = 0
            ttl_pl_sum = Counter(
                {"battles": 0, "dmg": 0, "spots": 0, "kills": 0, "dcp": 0, "wins": 0}
            )
            ttl_ex_sum = Counter(
                {"battles": 0, "dmg": 0, "spots": 0, "kills": 0, "dcp": 0, "wins": 0}
            )
            for tier, tanks in sorted(a_t.items(), reverse=False):
                tier_total = Counter()
                tier_tanks = []
                pl_sum = Counter(
                    {
                        "battles": 0,
                        "dmg": 0,
                        "spots": 0,
                        "kills": 0,
                        "dcp": 0,
                        "wins": 0,
                    }
                )
                ex_sum = Counter(
                    {
                        "battles": 0,
                        "dmg": 0,
                        "spots": 0,
                        "kills": 0,
                        "dcp": 0,
                        "wins": 0,
                    }
                )

                for tank_id, tank in tanks.items():
                    WN8 = 0
                    if tank["all"]["battles"]:
                        # self.bot.logger.info(("xxx", tank["name"]))
                        if averages_all.get(str(tank_id), None):
                            # self.bot.logger.info(("xxx", tank["name"]))
                            t = tank["all"]
                            a = averages_all[str(tank_id)]
                            try:
                                rDAMAGE = t["damage_dealt"] / (
                                    t["battles"] * (a["dmg"] / a["battles"])
                                )
                                rSPOT = t["spotted"] / (
                                    t["battles"] * (a["spots"] / a["battles"])
                                )
                                rFRAG = t["frags"] / (
                                    t["battles"] * (a["kills"] / a["battles"])
                                )
                                rDEF = t["dropped_capture_points"] / (
                                    t["battles"] * (a["dcp"] / a["battles"])
                                )
                                rWIN = t["wins"] / (
                                    t["battles"] * (a["wins"] / a["battles"])
                                )

                                rWINc = max(0, (rWIN - 0.71) / (1 - 0.71))
                                rDAMAGEc = max(0, (rDAMAGE - 0.22) / (1 - 0.22))
                                rFRAGc = max(
                                    0, min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12))
                                )
                                rSPOTc = max(
                                    0, min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38))
                                )
                                rDEFc = max(
                                    0, min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10))
                                )
                                WN8 = (
                                    980 * rDAMAGEc
                                    + 210 * rDAMAGEc * rFRAGc
                                    + 155 * rFRAGc * rSPOTc
                                    + 75 * rDEFc * rFRAGc
                                    + 145 * min(1.8, rWINc)
                                )
                            except:
                                WN8 = 0
                                self.bot.logger.info("2 WN8 err")

                            pl_sum += {
                                "battles": t["battles"],
                                "dmg": t["damage_dealt"],
                                "spots": t["spotted"],
                                "kills": t["frags"],
                                "dcp": t["dropped_capture_points"],
                                "wins": t["wins"],
                            }

                            ex_sum += {
                                "battles": a["battles"],
                                "dmg": a["dmg"],
                                "spots": a["spots"],
                                "kills": a["kills"],
                                "dcp": a["dcp"],
                                "wins": a["wins"],
                            }

                        tier_total += Counter(tank["all"])
                        if "xp" not in tier_total:
                            tier_total["xp"] = 0
                        try:
                            tier_tanks.append(
                                [
                                    tank["name"][:12],
                                    tank["all"]["battles"],
                                    round(
                                        tank["all"]["wins"]
                                        / float(tank["all"]["battles"])
                                        * 100,
                                        2,
                                    ),
                                    round(
                                        tank["all"]["damage_dealt"]
                                        / float(tank["all"]["battles"]),
                                        2,
                                    ),
                                    WN8,
                                ]
                            )
                        except:
                            self.bot.logger.info(("2.1 WN8 err", tank["name"]))

                            tier_tanks.append(
                                [tank["name"][:12], tank["all"]["battles"], 0, 0, WN8]
                            )

                t = pl_sum
                a = ex_sum
                try:
                    rDAMAGE = t["dmg"] / (t["battles"] * (a["dmg"] / a["battles"]))
                    rSPOT = t["spots"] / (t["battles"] * (a["spots"] / a["battles"]))
                    rFRAG = t["kills"] / (t["battles"] * (a["kills"] / a["battles"]))
                    rDEF = t["dcp"] / (t["battles"] * (a["dcp"] / a["battles"]))
                    rWIN = t["wins"] / (t["battles"] * (a["wins"] / a["battles"]))

                    rWINc = max(0, (rWIN - 0.71) / (1 - 0.71))
                    rDAMAGEc = max(0, (rDAMAGE - 0.22) / (1 - 0.22))
                    rFRAGc = max(0, min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12)))
                    rSPOTc = max(0, min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38)))
                    rDEFc = max(0, min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10)))
                    TWN8 = (
                        980 * rDAMAGEc
                        + 210 * rDAMAGEc * rFRAGc
                        + 155 * rFRAGc * rSPOTc
                        + 75 * rDEFc * rFRAGc
                        + 145 * min(1.8, rWINc)
                    )
                except:
                    TWN8 = 0
                    self.bot.logger.info("3 WN8 err")

                if tier_total.get("battles", None):
                    tier_total["winrate"] = round(
                        tier_total["wins"] / float(tier_total["battles"]) * 100, 2
                    )
                    tier_total["dpb"] = round(
                        tier_total["damage_dealt"] / float(tier_total["battles"]), 2
                    )
                    tier_total["wn8"] = round(TWN8, 2)

                all_total += tier_total
                ttl_pl_sum += pl_sum
                ttl_ex_sum += ex_sum

                if tier_total.get("battles", None):
                    embed.add_field(
                        name=_("Tier {}").format(tier),
                        inline=False,
                        value=_(
                            "```{color}\nBattles: {battles}, WR: {winrate}, DPB: {dpb}, WN8: {wn8}, XP: {xp}```"
                        ).format(color=color_tier, **dict(tier_total)),
                    )
                    embed_short.add_field(
                        name=_("Tier {}").format(tier),
                        inline=False,
                        value=_(
                            "```{color}\nBattles: {battles}, WR: {winrate}, DPB: {dpb}, WN8: {wn8}, XP: {xp}```"
                        ).format(color=color_tier, **dict(tier_total)),
                    )
                    char_counter += len(tabulate(tier_tanks))
                    embed.add_field(
                        name="\uFEFF",
                        inline=False,
                        value="```{color}\n{out}```\uFEFF\n".format(
                            color=color_tanks,
                            out=tabulate(
                                sorted(tier_tanks, key=lambda x: (x[2], x[3])),
                                headers=[
                                    _("Name"),
                                    _("Btls"),
                                    _("WR"),
                                    _("DPB"),
                                    _("WN8"),
                                ],
                                stralign="left",
                                floatfmt=".0f",
                                numalign="right",
                            ),
                        ),
                    )

            t = ttl_pl_sum
            a = ttl_ex_sum
            TTLWN8 = 0
            if a["battles"]:
                try:
                    rDAMAGE = t["dmg"] / (t["battles"] * (a["dmg"] / a["battles"]))
                    rSPOT = t["spots"] / (t["battles"] * (a["spots"] / a["battles"]))
                    rFRAG = t["kills"] / (t["battles"] * (a["kills"] / a["battles"]))
                    rDEF = t["dcp"] / (t["battles"] * (a["dcp"] / a["battles"]))
                    rWIN = t["wins"] / (t["battles"] * (a["wins"] / a["battles"]))

                    rWINc = max(0, (rWIN - 0.71) / (1 - 0.71))
                    rDAMAGEc = max(0, (rDAMAGE - 0.22) / (1 - 0.22))
                    rFRAGc = max(0, min(rDAMAGEc + 0.2, (rFRAG - 0.12) / (1 - 0.12)))
                    rSPOTc = max(0, min(rDAMAGEc + 0.1, (rSPOT - 0.38) / (1 - 0.38)))
                    rDEFc = max(0, min(rDAMAGEc + 0.1, (rDEF - 0.10) / (1 - 0.10)))
                    TTLWN8 = (
                        980 * rDAMAGEc
                        + 210 * rDAMAGEc * rFRAGc
                        + 155 * rFRAGc * rSPOTc
                        + 75 * rDEFc * rFRAGc
                        + 145 * min(1.8, rWINc)
                    )
                except:
                    TTLWN8 = 0
                    self.bot.logger.info("4 WN8 err")

            if all_total["battles"]:

                all_info_new = new_data["info_data"].get("all", None)
                all_info_old = old_data["info_data"].get("all", None)

                if all_info_old is not None and all_info_old is not None:
                    old_wins = all_info_old.get("wins", 0)
                    old_battles = all_info_old.get("battles", 0)
                    old_damage = all_info_old.get("damage_dealt", 0)

                    new_wins = all_info_new.get("wins", 0)
                    new_battles = all_info_new.get("battles", 0)
                    new_damage = all_info_new.get("damage_dealt", 0)

                    try:
                        old_wr = old_wins / old_battles * 100
                        old_dpb = old_damage / old_battles

                        new_wr = new_wins / new_battles * 100
                        new_dpb = new_damage / new_battles

                        all_total["delta_wr"] = new_wr - old_wr
                        all_total["delta_dpb"] = new_dpb - old_dpb
                    except ArithmeticError as e:
                        self.bot.logger.info(e)

                final_tanks = _(
                    "Battles: {battles}, WR: {winrate:.2f} ({delta_wr:+,.3f}), DPB: {dpb:.1f} ({delta_dpb:+,.2f}), WN8: {wn8:.1f}, XP: {xp}\n"
                ).format(
                    battles=all_total["battles"],
                    xp=all_total["xp"],
                    winrate=all_total["wins"] / all_total["battles"] * 100,
                    dpb=all_total["damage_dealt"] / all_total["battles"],
                    wn8=TTLWN8,
                    delta_wr=all_total.get("delta_wr", 0),
                    delta_dpb=all_total.get("delta_dpb", 0),
                )

                embed.add_field(name="\uFEFF", value="\n\uFEFF", inline=False)
                embed.add_field(
                    name=_("Total encounter battles:"),
                    value="```{color}\n{tanks}```\n\uFEFF".format(
                        color=color_total, tanks=final_tanks
                    ),
                    inline=False,
                )

                winrate_for_color = all_total["wins"] / all_total["battles"] * 100
                # embed.color = self.bot.wg.winrate_color(winrate)
                # embed_short.color = self.bot.wg.winrate_color(winrate)

                embed.color = self.bot.wg.wn8_color(TTLWN8)
                embed_short.color = self.bot.wg.wn8_color(TTLWN8)

                embed_short.add_field(
                    name=_("Total encounter battles:"),
                    value="```{color}\n{tanks}```\n\uFEFF".format(
                        color=color_total, tanks=final_tanks
                    ),
                    inline=False,
                )
                embed_short.set_footer(
                    text=_(
                        "Stats by \U0001f451 WotBot. Resets early morning for each region. Tier 0 are tanks not in tankopedia."
                    )
                )

            embed.set_footer(
                text=_(
                    "Stats by \U0001f451 WotBot. Resets early morning for each region. Tier 0 are tanks not in tankopedia."
                )
            )
        else:
            embed.add_field(
                name="\uFEFF", value=_("No encounter battles...\n\uFEFF"), inline=False
            )
        if rating_table:
            embed.add_field(
                name=_("Total rating battles:"),
                value="```{color}\n{rating}```\n\uFEFF".format(
                    color=color_total, rating=rating_table
                ),
                inline=False,
            )
            embed_short.add_field(
                name=_("Total rating battles:"),
                value="```{color}\n{rating}```\n\uFEFF".format(
                    color=color_total, rating=rating_table
                ),
                inline=False,
            )
            if winrate_for_color < rating_winrate:
                embed.color = self.bot.wg.winrate_color(rating_winrate)
                embed_short.color = self.bot.wg.winrate_color(rating_winrate)
        else:
            embed.add_field(
                name="\uFEFF", value=_("No rating battles...\n\uFEFF"), inline=False
            )

        if rating2:
            embed.add_field(
                name=_("Rating, current season:"),
                value=_(
                    '```{color}\nPoints: {0} Place: {1} League: "{2}"\nSlow status update by Wargaming```\n\uFEFF'
                ).format(color=color_total, *rating2),
                inline=False,
            )
            embed_short.add_field(
                name=_("Rating status:"),
                value=_(
                    '```{color}\nPoints: {0} Place: {1} League: "{2}"\nSlow status update by Wargaming```\n\uFEFF'
                ).format(color=color_total, *rating2),
                inline=False,
            )
        if achievs_table:
            embed.add_field(
                name=_("Achievements"),
                value="```{color}\n{achiev}```\n\uFEFF".format(
                    color=color_achievs,
                    achiev=tabulate(
                        sorted(achievs_table, key=lambda x: (x[1])),
                        headers=[_("name"), _("count")],
                    ),
                ),
                inline=False,
            )

        if mastery_table:
            mastery_marks = {
                4: _("Ace Tanker"),
                3: _("1st Class"),
                2: _("2nd Class"),
                1: _("3rd Class"),
                0: 0,
                None: None,
            }
            new_mastery_table = []
            for m, v in mastery_table.items():
                if mastery_marks.get(m, ""):
                    new_mastery_table.append([mastery_marks.get(m, ""), v])
            if new_mastery_table:
                embed.add_field(
                    name=_("Mastery"),
                    value="```{}```\n\uFEFF".format(
                        tabulate(
                            sorted(new_mastery_table, key=lambda x: (x[1])),
                            headers=[_("name"), _("count")],
                        )
                    ),
                    inline=False,
                )
                embed.color = colour.Color.dark_gold()
                embed.set_thumbnail(
                    url="https://cdn.discordapp.com/attachments/370945953550696449/479506499199172609/cup-icon1.png"
                )

        if ownuser and new_data.get("token_expired", False):
            embed.add_field(
                name=_("Daily resources stats"),
                value=_(
                    "Your authorization token has expired, please re-confirm your WarGaming account through the [WotBot Dashboard](https://wotbot.pythonanywhere.com/)"
                ),
                inline=False,
            )
            embed_short.add_field(
                name=_("Daily resources stats"),
                value=_(
                    "Your authorization token has expired, please re-confirm your WarGaming account through the [WotBot Dashboard](https://wotbot.pythonanywhere.com/)"
                ),
                inline=False,
            )
        if not ownuser:
            embed.add_field(
                name=_("Daily resources stats"),
                value=_(
                    "WotBot can calculate your daily Credits, Gold and Free XP: Go to [WotBot Dashboard](https://wotbot.pythonanywhere.com/), Sign in with Discord, go to Settings, Continue, then use the 'Link your WG account'. From next day on, WotBot will send you a Direct message containing value and delta for gold, credits and free xp whenever you use the 'daily' command."
                ),
                inline=False,
            )
            embed_short.add_field(
                name=_("Daily resources stats"),
                value=_(
                    "WotBot can calculate your daily Credits, Gold and Free XP, you must confirm your WarGaming account through the [WotBot Dashboard](https://wotbot.pythonanywhere.com/)"
                ),
                inline=False,
            )

        embed.add_field(
            inline=False,
            name="\uFEFF",
            value=":arrow_backward: {today} :left_right_arrow: {twodays} :arrow_forward: {yesterday}".format(
                today=_("Today"),
                yesterday=_("Yesterday"),
                twodays=_("Today & Yesterday"),
            ),
        )
        return (embed, embed_short, res_embed)

    @commands.command(
        pass_context=True,
        hidden=False,
        aliases=[
            "d",
            "dailystats",
            "recent",
            "recents",
            "daily.",
            "dialy",
            "daily?",
            "deily",
            "dsily",
            "today",
            "dailx",
            "yesterday",
            "battles",
        ],
    )
    async def daily(self, ctx, *, player_name: str = None):
        """Show daily statistics."""
        self.bot.logger.info(
            "start daily: {0} {1.name} {1.id}".format(player_name, ctx.message.author)
        )

        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return

        # self.bot.logger.info("daily: {}".format(player))

        embed, embed_short, res_embed = await self.calc_daily(
            player, region, ctx, "today", token=token, ownuser=ownuser
        )
        if embed is None:
            return
        msg = None
        private_msg = None
        if res_embed:
            private_msg = await ctx.message.author.send(content=None, embed=res_embed)
        try:
            msg = await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            try:
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
            except:
                await ctx.message.author.send(
                    content=_(
                        "Cannot send any message, please enable Sending messages/Embed links permission for wotbot."
                    )
                )

        except Exception as e:
            self.bot.logger.info(e)
            msg = await ctx.send(content=None, embed=embed_short)

        try:
            if msg is not None:
                await msg.add_reaction("\U000025c0")
                await asyncio.sleep(0.5)
                await msg.add_reaction("\U00002194")
                await msg.add_reaction("\U000025b6")
        except discord.Forbidden:
            self.bot.logger.warning(
                "Please enable Add reactions permission for wotbot."
            )
            await ctx.send(
                content=_("Please enable Add reactions permission for WotBot.")
            )

        if msg is None:
            return

        await asyncio.sleep(0.3)

        def check(reaction, user):
            if msg.id == reaction.message.id:
                if user != msg.author:
                    return reaction, user

        while True:
            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add", check=check, timeout=60
                )
                if reaction.emoji == "\U000025c0":
                    embed, embed_short, res_embed = await self.calc_daily(
                        player, region, ctx, "today", token=token, ownuser=ownuser
                    )
                    if res_embed:
                        try:
                            await private_msg.edit(content=None, embed=res_embed)
                        except Exception as e:
                            self.bot.logger.info(e)
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        try:
                            await msg.edit(content=None, embed=embed_short)
                        except Exception as e:
                            self.bot.logger.info(e)
                elif reaction.emoji == "\U000025b6":
                    embed, embed_short, res_embed = await self.calc_daily(
                        player, region, ctx, "yesterday", token=token, ownuser=ownuser
                    )
                    if res_embed:
                        try:
                            await private_msg.edit(content=None, embed=res_embed)
                        except Exception as e:
                            self.bot.logger.info(e)
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        try:
                            await msg.edit(content=None, embed=embed_short)
                        except Exception as e:
                            self.bot.logger.info(e)
                elif reaction.emoji == "\U00002194":
                    embed, embed_short, res_embed = await self.calc_daily(
                        player, region, ctx, "twodays", token=token, ownuser=ownuser
                    )
                    if res_embed:
                        try:
                            await private_msg.edit(content=None, embed=res_embed)
                        except Exception as e:
                            self.bot.logger.info(e)
                    try:
                        await msg.edit(content=None, embed=embed)
                    except Exception as e:
                        self.bot.logger.info(e)
                        try:
                            await msg.edit(content=None, embed=embed_short)
                        except Exception as e:
                            self.bot.logger.info(e)
            except asyncio.TimeoutError:
                try:
                    await msg.remove_reaction("\U000025c0", msg.author)
                    await asyncio.sleep(0.2)
                    await msg.remove_reaction("\U000025b6", msg.author)
                    await asyncio.sleep(0.5)
                    await msg.remove_reaction("\U00002194", msg.author)
                    await asyncio.sleep(0.2)
                    await msg.add_reaction("\U00002611")
                except Exception as e:
                    self.bot.logger.info(e)
                break
            self.bot.logger.info("exit")


def setup(bot):
    bot.add_cog(UserStats(bot))
