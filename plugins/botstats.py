import discord
from discord.ext import commands

# import asyncio
from operator import itemgetter
import random

# import requests
import json
import math
from tabulate import tabulate
import humanize
import datetime


class Utilities(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    def count_members(self):
        """List servers with wotbot."""
        online = 0
        offline = 0
        idle = 0
        bots = 0
        for server in self.bot.guilds:
            for member in server.members:
                if member.status == discord.Status.online:
                    online += 1
                elif member.status == discord.Status.offline:
                    offline += 1
                elif member.status == discord.Status.idle:
                    idle += 1
                if member.bot:
                    bots += 1
        return (online, offline, idle, bots)

    def list_online(self):
        """List servers with wotbot."""
        online = 0
        offline = 0
        idle = 0
        other = 0
        servers = 0
        for server_id in self.bot.ServerCfg:
            if "platoon-channel" in self.bot.ServerCfg[server_id]:
                server = self.bot.get_guild(int(server_id))
                if server is not None:
                    self.bot.logger.info(server.name)
                    servers += 1
                    for member in server.members:
                        if member.status == discord.Status.online:
                            online += 1
                        elif member.status == discord.Status.offline:
                            offline += 1
                        elif member.status == discord.Status.idle:
                            idle += 1
                        else:
                            other += 1
        self.bot.logger.info((servers, online, offline, idle, other))
        return (servers, online, offline, idle, other)

    @commands.command(pass_context=True, hidden=False, aliases=["botinfo", "infobot"])
    # @asyncio.coroutine
    async def botstats(self, ctx):
        """WotBot statistics."""
        self.bot.logger.info("botstats")
        data = []
        data2 = []
        data3 = []
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)

        members_total = 0
        for server in self.bot.guilds:
            members_total += server.member_count
        data.append([_("Servers"), len(self.bot.guilds)])
        count_members = self.count_members()
        data.append([_("Total members"), members_total])
        data.append([_("People"), members_total - count_members[3]])
        data.append([_("Bots"), count_members[3]])
        data.append([_("Members online"), count_members[0]])
        data.append([_("Members offline"), count_members[1]])
        data.append([_("Members idle"), count_members[2]])

        # platoon network
        count = self.list_online()
        idle = ""
        other = ""
        if count[3] > 0:
            idle = "{} idle".format(count[3])
        if count[4] > 0:
            other = "{} other".format(count[4])
        data2.append([_("Servers"), count[0]])
        data2.append([_("Members"), "{0[1]} online".format(count)])
        data2.append([_("Members"), "{0}".format(idle)])

        uptime = datetime.datetime.now() - self.bot.uptime
        uptime = humanize.naturaltime(uptime)
        data3.append(["Last restart", uptime])

        data3.append([_("Commands"), len(self.bot.commands)])
        table_stats = tabulate(
            data, floatfmt=".2f", numalign="right", tablefmt="grid", stralign="right"
        )
        table_stats2 = tabulate(
            data2, floatfmt=".2f", numalign="right", tablefmt="grid", stralign="right"
        )
        table_stats3 = tabulate(
            data3, floatfmt=".2f", numalign="right", tablefmt="grid", stralign="right"
        )

        data1 = await self.bot.sqlite.popular_commands(limit=5)
        self.bot.logger.debug(data1)
        table_commands1 = ""
        if len(data1):
            table_commands1 = "{}".format(
                tabulate(
                    data1,
                    headers=[_("Command"), _("Count")],
                    tablefmt="grid",
                    stralign="right",
                    numalign="right",
                )
            )

        data2 = await self.bot.sqlite.popular_commands2(limit=10, days=30)
        table_commands2 = ""
        if len(data2):
            table_commands2 = "{}".format(
                tabulate(
                    data2,
                    headers=[_("Command"), _("Count")],
                    tablefmt="grid",
                    stralign="right",
                    numalign="right",
                )
            )

        data3 = await self.bot.sqlite.popular_commands2(limit=10, days=7)
        table_commands3 = ""
        if len(data3):
            table_commands3 = "{}".format(
                tabulate(
                    data3,
                    headers=[_("Command"), _("Count")],
                    tablefmt="grid",
                    stralign="right",
                    numalign="right",
                )
            )

        data4 = await self.bot.sqlite.popular_commands2(limit=10, days=1)
        table_commands4 = ""
        if len(data4):
            table_commands4 = "{}".format(
                tabulate(
                    data4,
                    headers=[_("Command"), _("Count")],
                    tablefmt="grid",
                    stralign="right",
                    numalign="right",
                )
            )
        embed = discord.Embed(
            title=_("WotBot stats for geeks"), description=None, colour=234, type="rich"
        )
        embed.add_field(
            name=_("Uptime and commands"),
            value="```{}```".format(table_stats3),
            inline=False,
        )
        embed.add_field(
            name=_("Servers and members"),
            value="```{}```".format(table_stats),
            inline=False,
        )
        embed.add_field(
            name=_("Platoon connector"),
            value="```{}```".format(table_stats2),
            inline=False,
        )
        embed.add_field(
            name=_("Most popular commands ever"),
            value="```{}```".format(table_commands1),
            inline=False,
        )
        embed.add_field(
            name=_("Most popular commands last month"),
            value="```{}```".format(table_commands2),
            inline=False,
        )
        embed.add_field(
            name=_("Most popular commands last week"),
            value="```{}```".format(table_commands3),
            inline=False,
        )
        embed.add_field(
            name=_("Most popular commands last day"),
            value="```{}```".format(table_commands4),
            inline=False,
        )
        embed.set_thumbnail(
            url="https://cdn.discordapp.com/avatars/368866137603309569/957dff2d97950943a43127e1561d0409.png?size=256"
        )
        embed.set_footer(
            text="Lovely recursion: stats about bot about stats.",
            icon_url="https://cdn.discordapp.com/avatars/368866137603309569/957dff2d97950943a43127e1561d0409.png?size=128",
        )
        try:
            await ctx.send(content=None, embed=embed)
        except discord.Forbidden:
            self.bot.logger.warning("Please enable Embed links permission for wotbot.")
            try:
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
            except discord.Forbidden:
                self.bot.logger.warning("Cannot send any message!!!")


def setup(bot):
    bot.add_cog(Utilities(bot))
