import discord
from discord.ext import commands

# import asyncio
from operator import itemgetter
import random


import aiohttp
import tempfile

region_wg_to_bs = {"na": "com", "eu": "eu", "ru": "ru", "asia": "asia"}


class BlitzStarsStats(commands.Cog):
    """Staistics and signatures from BlitzStars"""

    def __init__(self, bot):
        self.bot = bot


    @commands.command(pass_context=True, aliases=["blitzsig", "blitzsig2", "bsigmeme"])
    # @asyncio.coroutine
    async def bsig(self, ctx, *, player_name: str = None):
        """BlitzStars WR signature."""
        _ = self.bot.lang[self.bot.set_lang(ctx)].gettext
        await self.bot.dc.typing(ctx)
        print(ctx.message.content)

        player, region, token, ownuser = await self.bot.wg.get_local_player(
            player_name, ctx
        )

        if not player:
            return
        self.bot.logger.info(("bsig:", player_name))

        # self.bot.logger.debug((player, region))
        if player:
            player_id = str(player[0]["account_id"])
            player_nick = player[0]["nickname"]
            _hash = random.getrandbits(128)
            # self.bot.logger.debug(_hash)
            # self.bot.logger.debug("https://www.blitzstars.com/api/sigs/eu/{}.png".format(player_id))
            img_url = "https://www.blitzstars.com/api/sigs/{}/{}.png?{}".format(
                region_wg_to_bs[region], player_id, _hash
            )
            self.bot.logger.debug(img_url)
            title_text = _("BlitzStars statistics")
            embed = discord.Embed(
                title="{title} {for_} `{nick}@{region}`\n{confirmed}\n{contributor}".format(
                    title=title_text,
                    for_=_("for"),
                    nick=player_nick,
                    region=region,
                    confirmed=await self.bot.dc.confirmed(ctx, ownuser),
                    contributor=await self.bot.dc.contributor(ctx, player, region),
                ),
                description="",
                colour=234,
                type="rich",
                url=await self.bot.dc.confirmed_url(ctx, ownuser),
                # url="https://www.blitzstars.com/player/{}/{}".format(
                #    region_wg_to_bs[region], player_nick
                # ),
            )
            embed.set_image(url=img_url)
            embed.set_author(
                name="BlitzStars",
                url="https://www.blitzstars.com/",
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )
            embed.set_thumbnail(
                url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png"
            )
            embed.set_footer(
                text="Signature by BlitzStars",
                icon_url="https://www.blitzstars.com/assets/images/TankyMcPewpew.png",
            )
            try:
                await ctx.send(content=None, embed=embed)
            except discord.Forbidden:
                self.bot.logger.warning(
                    "Please enable Embed links permission for wotbot."
                )
                await ctx.send(
                    content=_("Please enable Embed links permission for wotbot.")
                )
        else:
            out = await self.bot.wg.search_player_a(ctx, player_name)
            await self.bot.dc.not_found_msg(ctx, out)


def setup(bot):
    bot.add_cog(BlitzStarsStats(bot))
