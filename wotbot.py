import discord
from discord.ext import commands
import random
import asyncio
import json
import sys
import datetime
from utils import models
from utils import wg_utils
from utils import dc_utils
from utils import gamedata
from utils import banned
import json
import humanize
import logging
#from utils import formatter
#from utils import webformatter
import aiohttp
from flufl.lock import Lock  # locking

# for bot overload:
import inspect
import importlib
import traceback
import gettext
import os
import sys
from pathlib import Path

# from discord.ext.commands.view import StringView
# from discord.ext.commands.context import Context



with open("data/config.json") as json_data_file:
    cfg = json.load(json_data_file)

with open("data/servers.json") as json_data_file:
    ServerCfg = json.load(json_data_file)

with open("data/servers_ch.json") as json_data_file:
    ServerCfgCh = json.load(json_data_file)

with open("data/players.json") as json_data_file:
    PlayerCfg = json.load(json_data_file)



# discord log
logger = logging.getLogger("discord")
logger.setLevel(logging.INFO)

# file log
handler = logging.FileHandler(filename="logs/discord.log", encoding="utf-8", mode="a")
handler.setFormatter(
    logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")
)

# console log
formatter2 = logging.Formatter(
    fmt="%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s %(lineno)d: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
handler2 = logging.StreamHandler()
handler2.setFormatter(formatter2)

if cfg["ProfileMemory"] == "True":
    import tracemalloc

    tracemalloc.start()

logger.addHandler(handler)
logger.addHandler(handler2)

# wotbot log
wblogger = logging.getLogger("wotbot")
wblogger.setLevel(cfg["Logging"])


# file log
wbhandler = logging.FileHandler(filename="logs/wotbot.log", encoding="utf-8", mode="a")
wbhandler.setFormatter(
    logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")
)

# console log
wbformatter2 = logging.Formatter(
    fmt="%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s %(lineno)d: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)
wbhandler2 = logging.StreamHandler()
wbhandler2.setFormatter(wbformatter2)
wblogger.addHandler(wbhandler)
wblogger.addHandler(wbhandler2)


discord_id = cfg["DiscordToken"]


def save_settings_ch():
    with open("data/servers_ch.json", "w") as json_data_file:
        json.dump(bot.ServerCfgCh, json_data_file)


def save_settings():
    lock = Lock("data/servers.json.lock")
    with lock:
        with open("data/servers.json", "w") as json_data_file:
            json.dump(bot.ServerCfg, json_data_file)


def save_player_settings():
    lock = Lock("data/players.json.lock")
    with lock:
        with open("data/players.json", "w") as json_data_file:
            json.dump(bot.PlayerCfg, json_data_file)


description = """Hi WotBot - WOT Blitz bot - here.
    See source, help or report bugs:  https://codeberg.org/vanous/wotbot/issues
    Discuss at http://forum.wotblitz.eu/index.php?showtopic=45862

    If not specified, regions are searched in order: eu, ru, na, asia.
    Player/Clan names: can be defined also as player@region → player@eu, clan@na
    
    Admins: look at ?help conf   to see possible server configuration.
    Users: use ?help conf playername   to set nick alias.

    There are number of commands here:"""
prefix = cfg["Prefix"]


def get_prefix(bot, msg):
    # here we implement per server prefixes
    if msg.guild is not None:
        bot.logger.debug(msg.guild)
        bot.logger.debug(msg.guild.id)
        if str(msg.guild.id) in bot.ServerCfg:
            cfg_prefix = bot.ServerCfg[str(msg.guild.id)].get("prefix", None)
            if cfg_prefix is not None:
                bot.logger.debug(("prefix:", cfg_prefix))
                return [
                    cfg_prefix,
                    "{0.user.mention} ".format(bot),
                    "<@!{0.user.id}> ".format(bot),
                ]
    return [prefix, "{0.user.mention} ".format(bot), "<@!{0.user.id}> ".format(bot)]
    # if a callable is used:
    # return commands.when_mentioned_or(*[prefix, "{0.user.mention} ".format(bot), "<@!{0.user.id}> ".format(bot)])(bot,msg)

intents = discord.Intents.default()
intents.members = True

bot = commands.AutoShardedBot(
    command_prefix=get_prefix,
    intents=intents,
    description=description,
    #formatter=formatter.AryasFormatter(width=100),
    case_insensitive=True,
    owner_id=int(cfg["OwnerID"]),
)
# bot = commands.Bot(command_prefix=get_prefix, description=description, formatter=formatter.AryasFormatter(width=100), case_insensitive=True,owner_id=cfg["OwnerID"])

bot.logger = wblogger


def set_lang_member(member):
    # print("setting lang for ", member.id, member.guild.id)
    author_lang = None
    server_lang = None
    if str(member.id) in bot.PlayerCfg:
        author_lang = bot.PlayerCfg[str(member.id)].get("language", None)
    if str(member.guild.id) in bot.ServerCfg:
        server_lang = bot.ServerCfg[str(member.guild.id)].get("language", None)
    if author_lang:
        bot.logger.info("user {}".format(author_lang))
        return author_lang
    elif server_lang:
        bot.logger.info("server {}".format(server_lang))
        return server_lang
    else:
        # bot.logger.info("default EN")
        return "en"


def set_lang(ctx):
    # print("setting lang")
    author_lang = None
    server_lang = None
    if str(ctx.message.author.id) in bot.PlayerCfg:
        author_lang = bot.PlayerCfg[str(ctx.message.author.id)].get("language", None)
    if ctx.message.guild:
        if str(ctx.message.guild.id) in bot.ServerCfg:
            server_lang = bot.ServerCfg[str(ctx.message.guild.id)].get("language", None)
    if author_lang:
        bot.logger.info("user {}".format(author_lang))
        return author_lang
    elif server_lang:
        bot.logger.info("server {}".format(server_lang))
        return server_lang
    else:
        # bot.logger.info("default EN")
        return "en"


bot.remove_command("help")
bot.cfg = cfg
bot.ServerCfgCh = ServerCfgCh
bot.ServerCfg = ServerCfg
bot.PlayerCfg = PlayerCfg
bot.lang = {}


def start_lang():
    gettext._translations = {}
    bot.lang["en"] = gettext.translation(
        "messages", localedir="translations", languages=["en"]
    )
    bot.lang["cs"] = gettext.translation(
        "messages", localedir="translations", languages=["cs"]
    )
    bot.lang["ru"] = gettext.translation(
        "messages", localedir="translations", languages=["ru"]
    )
    bot.lang["fr"] = gettext.translation(
        "messages", localedir="translations", languages=["fr"]
    )
    bot.lang["nl"] = gettext.translation(
        "messages", localedir="translations", languages=["nl"]
    )

    bot.lang["hu"] = gettext.translation(
        "messages", localedir="translations", languages=["hu"]
    )
    bot.lang["de"] = gettext.translation(
        "messages", localedir="translations", languages=["de"]
    )
    bot.lang["az"] = gettext.translation(
        "messages", localedir="translations", languages=["az"]
    )

    bot.lang["pl"] = gettext.translation(
        "messages", localedir="translations", languages=["pl"]
    )

    bot.lang["it"] = gettext.translation(
        "messages", localedir="translations", languages=["it"]
    )
    bot.lang["es"] = gettext.translation(
        "messages", localedir="translations", languages=["es"]
    )
    bot.lang["ar"] = gettext.translation(
        "messages", localedir="translations", languages=["ar"]
    )
    bot.lang["tr"] = gettext.translation(
        "messages", localedir="translations", languages=["tr"]
    )
    bot.lang["zh"] = gettext.translation(
        "messages", localedir="translations", languages=["zh"]
    )
start_lang()
bot.start_lang = start_lang
bot.set_lang = set_lang
bot.set_lang_member = set_lang_member

bot.save_settings = save_settings
bot.save_player_settings = save_player_settings
bot.save_settings_ch = save_settings_ch


bot.devel = False
if cfg["Devel"] == "True":
    bot.devel = True
    bot.logger.info("DEVEL")


@bot.event
async def playing():
    await bot.change_presence(
        activity=discord.Activity(
            name="{0}help @ {1}servers".format(prefix, len(bot.guilds)), type=2
        )
    )


@bot.event
async def on_ready():
    bot.logger.info("Logged in as")
    bot.logger.info(bot.user.name)
    bot.logger.info(bot.user.id)
    await playing()


@bot.event
async def on_guild_join(guild):
    bot.logger.info("Added by server {}".format(guild.name))
    await playing()


@bot.event
async def on_guild_remove(guild):
    bot.logger.info("Removed by server {}".format(guild.name))
    await playing()


async def remove_account_data(data):
    bot.logger.info("remove this: {}".format(data))
    if data.get("user_id", False):
        confirmedaccount_id = data.get("confirmedaccount_id", None)
        bot.PlayerCfg.pop(data["user_id"],False)
        save_player_settings()
        await bot.sqlite.remove_account(data)
        p=Path()
        for statfile in p.glob('stats/{}*'.format(data["user_id"])):
            print(statfile)
            Path.unlink(statfile)
        if data.get("confirmedaccount_id",False):
            for statfile in p.glob('stats/*-{}*'.format(data["confirmedaccount_id"])):
                print(statfile)
                Path.unlink(statfile)

        for acefile in p.glob('stats/*aceheroes.json'):
            acedirty=False
            print(acefile)
            with open(acefile) as json_data_file:
                acedata = json.load(json_data_file)

            if acedata.get("allowed", False):
                for allowed in acedata.get("allowed",[]):
                    if data["user_id"] in allowed:
                        acedata["allowed"].remove(allowed)
                        acedirty=True

            if acedata.get("banned", False):
                for banned in acedata.get("banned",[]):
                    if data["user_id"] in banned:
                        acedata["banned"].remove(banned)
                        acedirty=True

            if acedata.get("managers", False):
                for manager in acedata.get("managers",[]):
                    if data["user_id"] in manager:
                        acedata["managers"].remove(manager)
                        acedirty=True
            
            if acedata.get("data", False):
                for tier,record in acedata["data"].items():
                    for tank in record:
                        if tank.get("approved",False):
                            if data["user_id"] in tank["approved"]["id"]:
                                tank["approved"]["id"]="0"
                                tank["approved"]["name"]="removed"
                                acedirty=True

                        if tank.get("submitter",False):
                            if data["user_id"] in tank["submitter"]["id"]:
                                tank["submitter"]["id"]="0"
                                tank["submitter"]["name"]="removed"
                                acedirty=True
            if acedirty:
                print("remove from {}".format(acefile))
                lock = Lock("{acefile}.lock".format(acefile=acefile))
                lock.lock()

                with open(
                    acefile, "w"
                ) as json_data_file:
                    json.dump(acedata, json_data_file)
                lock.unlock()

async def store_dashboard_data(data, data2):
    bot.logger.info("save this: {}".format(data))
    # user
    if data.get("userid", False):
        if data.get("user", False):
            userdata = data["user"]
            confirmedname = userdata.pop("confirmedname", None)
            confirmedtoken = userdata.pop("confirmedtoken", None)
            confirmedaccount_id = userdata.get("confirmedaccount_id", None)
            confirmedregion = userdata.get("confirmedregion", None)

            bot.PlayerCfg[data["userid"]] = userdata
            save_player_settings()
            if (
                confirmedaccount_id is not None
                and confirmedname is not None
                and confirmedregion != ""
                and confirmedregion is not None
                and confirmedtoken is not None
            ):
                print("saving token")
                await bot.sqlite.add_user(
                    confirmedaccount_id, confirmedname, confirmedregion, confirmedtoken
                )

        if data.get("guilds", False):
            for guild in data.get("guilds"):
                data_str = json.dumps(guild["config"])
                bot.ServerCfg[guild["id"]] = json.loads(data_str.replace("_", "-"))

                cfg_prefix = bot.ServerCfg[guild["id"]].get("prefix", None)
                if cfg_prefix is not None:
                    if cfg_prefix == "":
                        bot.ServerCfg[guild["id"]]["prefix"] = "?"
                        bot.logger.info("fixing prefix!")

            save_settings()
        await collect_data(data2)


async def collect_data(data):
    return_guilds = []
    return_guilds_invite = []
    return_playerconfigs = {}
    return_blitzstats = {}
    if data.get("guilds", False):
        guilds = data.get("guilds")
        if len(guilds):

            for guild in guilds:
                server = bot.get_guild(int(guild["id"]))

                if server:
                    channels = []
                    for channel in server.channels:
                        if isinstance(channel, discord.TextChannel):
                            channels.append(
                                {"name": channel.name, "id": str(channel.id)}
                            )
                    server_config = {}
                    if guild["id"] in bot.ServerCfg:
                        server_config = json.loads(
                            json.dumps(bot.ServerCfg[guild["id"]]).replace("-", "_")
                        )

                    return_guilds.append(
                        {
                            "name": server.name,
                            "id": str(server.id),
                            "channels": channels,
                            "config": server_config,
                        }
                    )
                else:
                    return_guilds_invite.append(
                        {"name": guild["name"], "id": guild["id"]}
                    )

    if data.get("user_id", None) is not None:
        if data.get("user_id") in bot.PlayerCfg:
            return_playerconfigs = bot.PlayerCfg[data.get("user_id")]
            # print("1",return_playerconfigs)
            confirmeddata = await bot.sqlite.get_user(
                return_playerconfigs.get("confirmedaccount_id", None)
            )
            # print("from db",confirmeddata)
            if confirmeddata:
                return_playerconfigs["confirmedtoken"] = confirmeddata[2]
                return_playerconfigs["confirmedname"] = confirmeddata[3]
                # print("2",return_playerconfigs)

        bq_res = await bot.sqlite.get_quiz(data.get("user_id"))
        if bq_res:
            return_blitzstats = bq_res

    payload = {
        "dashboard_key": cfg["DashboardKey"],
        "user_id": data["user_id"],
        "guilds": return_guilds,
        "playerconfigs": return_playerconfigs,
        "guilds_invite": return_guilds_invite,
        "blitzquiz": return_blitzstats,
    }
    headers = {"Content-Type": "application/json"}
    #bot.logger.info("data to be sent back: {}".format(json.dumps(payload)))

    url = cfg["DashboardReceiverUrl"]
    async with aiohttp.ClientSession(loop=bot.loop) as client:
        async with client.post(
            url, data=json.dumps(payload), headers=headers
        ) as response:
            bot.logger.info("response: {}".format(response.status))
            t = await response.text()
            bot.logger.info(t)


@bot.event
async def on_message(message):
    # do some extra stuff here
    # bot.logger.debug((message.server, message.channel, message.author.name, message.content))
    # bot.logger.debug(('bot is talking to me!', message.author.id, message.author.name, message.content))
    # print(message.guild.id, message.channel.id,message.author.id)
    bot.watchdog = True
    if message.guild is not None:
        if str(message.guild.id) == cfg["DashboardServerID"]:
            if str(message.channel.id) == cfg["DashboardChannelID"]:
                bot.logger.info("webhook msg author: {}".format(message.author.id))
                bot.logger.info("msg author bot: {}".format(message.author.bot))
                if str(message.author.id) == cfg["DashboardAuthorID"]:
                    data = json.loads(message.content)
                    data_key = data.get("dashboard_key", False)
                    print("GOT HERE", data_key)
                    if data_key == cfg["DashboardKey"]:
                        data_command = data.get("command", False)
                        if data_command == "remove-data":
                            print(
                                "Received from dashboard remove-data:",
                                json.loads(json.dumps(data)),
                            )
                            await remove_account_data(data)
                        if data_command == "fetch":

                            payload = {
                                "dashboard_key": cfg["DashboardKey"],
                                "user_id": data.get("user_id", 0),
                            }
                            headers = {"Content-Type": "application/json"}
                            bot.logger.info("submit: {}".format(json.dumps(payload)))

                            url = cfg["DashboardResponderUrl"]
                            async with aiohttp.ClientSession(loop=bot.loop) as client:
                                async with client.post(
                                    url, data=json.dumps(payload), headers=headers
                                ) as response:
                                    # t = await response.text()
                                    # print(t)
                                    data = await response.json()
                                    # bot.logger.info(data)
                            # data = json.loads(t)
                            data = data.get("content")
                            data_key = data.get("dashboard_key", False)
                            if data_key == cfg["DashboardKey"]:
                                data_command = data.get("command", False)

                                if data_command == "get-data":
                                    print(
                                        "Received from dashboard get-data:",
                                        json.loads(json.dumps(data)),
                                    )
                                    await collect_data(data)
                                if data_command == "set-data":
                                    print(
                                        "Received from dashboard set-data:",
                                        json.loads(json.dumps(data)),
                                    )
                                    await store_dashboard_data(
                                        json.loads(json.dumps(data["data"])), data
                                    )

    if not message.author.bot:
        if bot.user.mentioned_in(message):
            ctx = await bot.get_context(message)
            if not ctx.valid and not (
                "@here" in message.content or "@everyone" in message.content
            ):
                command = bot.all_commands["daily"]
                await ctx.invoke(command, player_name=message.content)
                return

        is_banned = bot.banned.is_discord_id_banned(message.author.id)
        if is_banned:
            banned_account = banned.DAccount(message.author.id, bot.banned)
            bot.logger.info(f"banned {banned_account}")
            ctx = await bot.get_context(message)
            ban_msg=f"Your Discord account in WotBot has been banned for some time for `{banned_account.reason.long_reason()}`."
            if len(banned_account.url) > 3:
                ban_msg += " " + f"Here is the replay: {banned_account.url}."
            ban_msg += " " + f" Next time, your Discord AND your WG account will be banned."
            await ctx.send(ban_msg)
            pass
        else:
            await bot.process_commands(message)

        if message.guild is not None:
            if str(message.guild.id) in bot.ServerCfg:
                if "replay-channel" in bot.ServerCfg[str(message.guild.id)]:
                    if (
                        str(message.channel.id)
                        in bot.ServerCfg[str(message.guild.id)]["replay-channel"]
                    ):
                        if message.attachments:
                            # print("auto upload channel",message.content,message.attachments)
                            for att in message.attachments:
                                if att.url.startswith("http") and att.url.endswith(
                                    "wotbreplay"
                                ):
                                    command = bot.all_commands["replay"]
                                    ctx = await bot.get_context(message)
                                    await ctx.invoke(
                                        command,
                                        url=att.url,
                                        title=message.content,
                                        stop=True,
                                    )
                                    await bot.sqlite.add_command(
                                        message.guild, "replay-autoupload"
                                    )
                            return
        else:  # DM

            if message.attachments:
                # print("auto upload channel",message.content,message.attachments)
                for att in message.attachments:
                    if att.url.startswith("http") and att.url.endswith("wotbreplay"):
                        command = bot.all_commands["replay"]
                        ctx = await bot.get_context(message)
                        await ctx.invoke(
                            command, url=att.url, title=message.content, stop=True
                        )
                        await bot.sqlite.add_command(message.guild, "replay-autoupload")
                return


@bot.event
async def on_command_error(ctx, exception):
    bot.logger.info(("on cmd error:", exception))
    try:
        await ctx.message.add_reaction("\U0001f914")
    except:
        pass  # not allowed, no harm to continue

    if isinstance(exception, commands.errors.CommandOnCooldown):
        await ctx.send(
            "Not so fast, try again in {minutes} minutes".format(
                minutes=round(exception.retry_after / 60)
            )
        )

    else:
        traceback.print_exception(
            type(exception), exception, exception.__traceback__, file=sys.stderr
        )


@bot.event
async def on_command(ctx):
    if ctx.message.author.bot:
        bot.logger.info(
            (
                "bot is talking to me!",
                ctx.message.author.id,
                ctx.message.author.name,
                ctx.message.content,
            )
        )
    # print("menioned?", ctx.mentioned_in(ctx.message),ctx.message)
    bot.logger.debug((ctx.command, ctx.message.guild))
    custom_message = False
    if custom_message:
        if ctx.message.guild is not None:
            server = ctx.message.guild.name
            if ctx.command.name in bot.all_commands:
                if "command_doesn_exist" not in ctx.command.name:
                    if (
                        str(ctx.message.guild.id) not in bot.ServerCfgCh
                    ):  # didn't send yet
                        servers_subscribed = []
                        for (
                            server_in_list
                        ) in (
                            bot.ServerCfg
                        ):  # the following few conditions check if not already subscribed
                            if "updates-channel" in bot.ServerCfg[server_in_list]:
                                servers_subscribed.append(server_in_list)
                        if str(ctx.message.guild.id) not in servers_subscribed:
                            # if True:

                            # embed = discord.Embed(title="Announcement from your friendly WotBot.", colour=1747484,type="rich")
                            # embed.add_field(name="\uFEFF", value="This is a message to your channel administrator about how you can improve your discord server platoon search experience:")
                            # embed.add_field(name="\uFEFF", value="WotBot has a platoon command, which allows your members to post and take platoon request, for details see: `?help platoon`\n\nThis platoon connector works across discord servers who use WotbBot. If you like to be part of this platooning network, you can simply configure WotBot to receive platoon requests into your (dedicated) channel, where these messages should go to.\n\nIt is done by running command: `?conf platoon-channel add` typically in your **#bot** or **#platoon** channel. The command must be ran by channel or server admin, normal users don't have permissions to do this. You can unsubscribe at anytime by running command: `?conf platoon-channel remove`. Please see `?help conf` or `?help conf platoon-channel` for further details.")
                            # embed.add_field(name="\uFEFF",value="Feel free to join conversation about WotBot on [EU forum](http://forum.wotblitz.eu/index.php?/topic/45862-discord-bot-wotbot/). With hope that it improves your tanking experience,\nKind Regards\nWotBot by b48g55m")
                            # embed.set_thumbnail(url="https://user-images.githubusercontent.com/3680926/34730158-e5bd5682-f55e-11e7-9ea4-b48a0e496bb6.png")
                            # embed.set_author(name="b48g55m", url="https://github.com/vanous/wotbot", icon_url="https://user-images.githubusercontent.com/3680926/34730388-887d20aa-f55f-11e7-875c-d714f4331bbb.jpg")
                            # embed.set_image(url="https://user-images.githubusercontent.com/3680926/34730158-e5bd5682-f55e-11e7-9ea4-b48a0e496bb6.png")
                            # embed.set_footer(text="These messages are sent very rarely.")

                            embed = discord.Embed(
                                title="WotBot news",
                                colour=discord.Colour(0x453BD3),
                                url="https://wotbot.pythonanywhere.com/",
                                description="Hello, this is your **WotBot** here. Bringing you some news from the WotBot development hangar.",
                            )
                            embed.set_author(
                                name="b48g55m",
                                url="https://wotbot.pythonanywhere.com/",
                                icon_url="https://cdn.discordapp.com/avatars/345299188986150923/8f6ea60cfb9004b097623907a19f0bbf.png?size=256",
                            )
                            embed.set_image(
                                url="https://user-images.githubusercontent.com/3680926/45116362-beb88b00-b152-11e8-948a-bd5ab7cb5145.png"
                            )
                            embed.set_footer(
                                text="News are sent infrequently. You can subscribe to WotBot News (updates-channel) in Dashboard.",
                                icon_url="https://user-images.githubusercontent.com/3680926/45113636-2e2a7c80-b14b-11e8-8cb3-05d7bff11370.png",
                            )
                            embed.add_field(
                                name="Rating battles",
                                value="Normally, all statistics tools concentrate on Random battles only. Uniquely, the daily command in WotBot has been able to calculate statistics for Rating battles for a while now, which allows you to watch your stats more closely so you can try to improve where needed. Hope you'll find it useful.",
                            )

                            embed.add_field(
                                name="WotBot Dashboard and WotB Widget",
                                value="[WotBot Dashboard](https://wotbot.pythonanywhere.com/) has become a popular tool to set your personal and server WotBot preferences. I am happy to see that this development effort has been useful. [WotB Widget](https://widget.pythonanywhere.com/) is much more specialized tool, used mainly by people who do live streaming on YouTube, Twitch or other places. It can calculate and display your current live statistics, like winrate, damage per battle or number of battles during your stream. It has been translated to multiple languages and can now also track your Rating battles.",
                            )

                            embed.add_field(
                                name="Looking for translators",
                                value="Thanks to multiple people (big kudos to all existing helpers!), the web interfaces (WotBot Dashboard and WotB Widget) and also several WotBot commands (Daily, Config, BlitzQuiz, Replays) are now available in multiple languages. I am looking for more helpers to allow other non English speakers to use and more understand the WotBot commands and thus to learn how to read the statistics and be better players. If you like to help, please come to push the translating effort - simply log to [Crowdin](https://crwd.in/wotbot) (online translation tool) and get translating right away. Or you can also just read-proof, correct and confirm translations... any help is welcome! If your language is not listed as available for translation, just ping me, i will add it, see contact below.",
                            )

                            embed.add_field(
                                name="WotBot",
                                value="Not only has WotBot been added by more then 1500 servers, but the usage of commands has also been going up. For a small author like me it is great to have a tool that people like. If you have an idea of what is missing, what could be made better or if you like to help with some coding in Python, stop by at the [WotBot Labs discord server](https://discordapp.com/invite/mzXYPVW).",
                            )

                            embed.add_field(
                                name="\uFEFF", value="Enjoy your tanking, **_b48g55m_**"
                            )
                            try:

                                await ctx.channel.send(content=None, embed=embed)
                                bot.ServerCfgCh[str(ctx.message.guild.id)] = {}
                                bot.logger.info("sending special greeting message")
                                save_settings_ch()
                            except discord.Forbidden:
                                bot.logger.warning(
                                    "Please enable Embed links permission for wotbot."
                                )

    if ctx.message.guild is not None:
        server = ctx.message.guild.name
    else:
        server = "DirectChannel"

    await bot.sqlite.add_command(server, ctx.command)


async def my_background_task():
    while True:
        bot.watchdog = False
        await asyncio.sleep(360)  # task runs every 5 minutes
        bot.logger.info(".....watchdog.....")
        if not bot.watchdog:
            if not bot.devel:
                bot.logger.info("########RESTART########")
                sys.exit()


bot.loop.create_task(my_background_task())  # async background task

extensions_list = cfg["Plugins"]
for ext_item in extensions_list:
    bot.load_extension("plugins.{}".format(ext_item))

bot.banned = banned.Banned()
bot.wg = wg_utils.WG(cfg["WargamingToken"], bot)
bot.dc = dc_utils.DC(bot)
bot.sqlite = models.DB(wblogger, bot)
bot.gamedata = gamedata.Data()
#bot.webhelp = webformatter.WebFormatter()
bot.uptime = datetime.datetime.now()
bot.run(discord_id)
