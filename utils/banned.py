import json
from enum import Enum

class Reason(Enum):
    TOXIC=1

    def long_reason(self):
        if self==self.TOXIC:
            return "toxic behavior"


class Banned:
    banned_accounts={}

    def __init__(self):
        with open("data/banned.json") as json_data_file:
             self.banned_accounts=json.load(json_data_file)
             print(self.banned_accounts)
             print("banned instantiated")

    def is_discord_id_banned(self, d_id):
        if self.banned_accounts.get(str(d_id), None) is not None:
            return True
        return False

class DAccount:
    discord_id=None
    wg_id=None
    reason=None
    url=None

    def __init__(self, d_id, banned):
        data=banned.banned_accounts.get(str(d_id), None)
        self.discord_id=d_id
        reason=Reason
        self.reason=reason(data.get("reason", 1))
        self.url=data.get("url", None)

    def __str__(self):
        return f"{self.discord_id}"
